import { handleMove, createBattle, isBattleFor, battleFor } from '../battle';
import { describe } from '../pokemon';
import { actionTypes as battleActionTypes } from '../state/battles';
import { actionTypes as botTypes, actions as botActions } from '../state/bot';

const BOTNAME = 'pokebot';

const messages = [
    [battleActionTypes.BATTLE_MOVE, /use\s+[\w-]*/ig],
    [battleActionTypes.BATTLE_START, /challenge\s+\w*/ig],
    ['DESCRIBE', /describe/ig]
];

export default store => next => action => {
    if (action.type === botTypes.BOT_INCOMING) {
        handleMessage(store, action.payload);
    }
    return next(action);
};

function handleMessage(store, msg) {
    const { rtm } = store.getState().bot;
    if (msg.text.indexOf(BOTNAME) !== -1) {
        const maybeAction = messages.filter(m => msg.text.search(m[1]) !== -1);

        if (maybeAction.length > 0) {
            const action = maybeAction[0][0];
            const regexp = maybeAction[0][1];
            const user = rtm.dataStore.getUserById(msg.user);

            switch (action) {
            case 'DESCRIBE':
                var battle = battleFor(user);
                if (battle) {
                    var pokemon = battle.player1.name === user.name ?
                    battle.pokemon1 : battle.pokemon2;
                    store.dispatch(botActions[botTypes.BOT_DM](user, describe(pokemon)));
                } else {
                    store.dispatch(botActions[botTypes.BOT_DM](user, 'can\'t find a battle for you!'));
                }
                break;
            case battleActionTypes.BATTLE_START:
                var opponent = rtm.dataStore.getUserByName(msg.text.match(regexp)[0].split(' ').slice(-1)[0]);

                if (opponent && !isBattleFor(user) && !isBattleFor(opponent)) {
                    createBattle(user, opponent);
                } else {
                    store.dispatch(botActions[botTypes.BOT_DM](user, 'You already have a battle! Finish that one first'));
                }
                break;
            case battleActionTypes.BATTLE_MOVE:
                if (isBattleFor(user)) {
                    handleMove(user, msg.text.match(regexp)[0].split(' ').slice(-1)[0]);
                } else {
                    store.dispatch(botActions[botTypes.BOT_DM](user,
                        `You don't currently have a battle.
                    Start one by wondering through tall grass, or challenge
                    another player with 'challenge <playername>'`));
                }
                break;
            default:
                return;
            }
        }
    }else{
        const user = rtm.dataStore.getUserById(msg.user);
        if (!isBattleFor(user)) {
            if(Math.random() < 0.1){
                //Start a random encounter battle
                var opponent = rtm.dataStore.getUserByName('pokebot');
                store.dispatch(botActions[botTypes.BOT_MESSAGE](msg.channel, 'A wild pokemon appeared!'));
                createBattle(user, opponent);
            }
        }
    }
}
