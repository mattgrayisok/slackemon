import { actionTypes, actions as audioActions } from '../state/audio';
import { actionTypes as battleActionTypes } from '../state/battles';

function handleAudioPlay({ user, sound }, { audio }) {
    if (audio.connectedUsers && audio.connectedUsers.hasOwnProperty(user)) {
        audio.connectedUsers[user].emit('play', sound);
    }
}

export default store => next => action => {
    switch (action.type) {
    case actionTypes.AUDIO_PLAY:
        handleAudioPlay(action.payload, store.getState());
        break;

    case battleActionTypes.BATTLE_WON:
        store.dispatch(audioActions[actionTypes.AUDIO_PLAY](action.payload.name, 'victory'));
        break;

    case battleActionTypes.BATTLE_LOST:
        store.dispatch(audioActions[actionTypes.AUDIO_PLAY](action.payload.name, 'stop'));
        break;

    }
    next(action);
};
