import { actions as battleActions, actionTypes as battleActionTypes, rightTurn } from '../state/battles';
import { actions as botActions, actionTypes as botTypes } from '../state/bot';
import sendStatusMessage from '../util/statusMessage';
import calculateDamage from '../util/damageCalculator';

function handleUpdate({ battles }, payload) {
    const theBattle = battles.filter(b => b.id === payload)[0];
    sendStatusMessage(theBattle, '@'+theBattle.player1.name);
    sendStatusMessage(theBattle, '@'+theBattle.player2.name);
}

function handleMove(store, action) {
    var battle = store.getState().battles.filter(b => b.player1.name === action.payload.player.name || b.player2.name === action.payload.player.name)[0];
    if (!rightTurn(battle, action.payload.player)) {
        store.dispatch(battleActions[battleActionTypes.BATTLE_WRONG_TURN](action.payload.player));
        return;
    } else {
        const player = action.payload.player;
        const move = action.payload.move;
        const pokemon = battle.player1.name === player.name ? battle.pokemon1 : battle.pokemon2;
        const opponent = battle.player1.name === player.name ? battle.player2 : battle.player1;
        const opponentPokemon = battle.player1.name === player.name ? battle.pokemon2 : battle.pokemon1;
        const  totalDamage = calculateDamage(pokemon, opponentPokemon, move);

        store.dispatch(botActions[botTypes.BOT_DM](opponent, `${pokemon.name} used move ${move.name} against ${opponentPokemon.name}, causing ${totalDamage} damage`));
        store.dispatch(botActions[botTypes.BOT_DM](player, `${pokemon.name} used move ${move.name} against ${opponentPokemon.name}, causing ${totalDamage} damage`));

    }
}

function handleBattleStart(store, action) {
    sendStatusMessage(action.payload, '@'+action.payload.player1.name);
    sendStatusMessage(action.payload, '@'+action.payload.player2.name);
    console.log(action.payload.turn);
    if (action.payload.turn === '1') {
        store.dispatch(botActions[botTypes.BOT_DM](action.payload.player1, 'it\'s your go first'));
    } else {
        store.dispatch(botActions[botTypes.BOT_DM](action.payload.player2, 'it\'s your go first'));
    }
}

export default store => next => action => {
    switch (action.type) {
    case battleActionTypes.BATTLE_MOVE:
        handleMove(store, action);
        break;
    case battleActionTypes.BATTLE_WRONG_TURN:
        store.dispatch(botActions[botTypes.BOT_DM](action.payload, 'it\'s the other players turn!'));
        return;
    case battleActionTypes.BATTLE_START:
        handleBattleStart(store, action);
        break;
    case battleActionTypes.BATTLE_UPDATE:
        handleUpdate(store.getState(), action.payload);
        break;
    case battleActionTypes.BATTLE_WON:
        store.dispatch(botActions[botTypes.BOT_DM](action.payload, 'You won the match!'));
        break;
    case battleActionTypes.BATTLE_LOST:
        store.dispatch(botActions[botTypes.BOT_DM](action.payload, 'Your pokemon fainted!'));
        break;
    }
    return next(action);
};
