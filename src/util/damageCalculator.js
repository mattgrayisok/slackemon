export default function calculateDamage(attacker, defender, move){

    let lv = 50; //The attacker's level
    let at = attacker.at;
    let df = defender.df;
    let damage = move.damage;

    let baseDamage = Math.min(997, Math.floor(Math.floor(Math.floor(2 * lv / 5 + 2) * Math.max(1, at) * damage / Math.max(1, df)) / 50)) + 2;

    return baseDamage;
}
