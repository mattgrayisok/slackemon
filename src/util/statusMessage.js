import fetch from 'node-fetch';

function getBattleStatusMessage(battle){

    const colours = {
        ok:'#227744',
        warning: '#dfa941',
        bad: '#992244'
    };

    const msg = [{
        'fallback': 'Pokemon are fighting!',
        'color': battle.pokemon1.hp < 50 ? battle.pokemon1.hp < 20 ? colours.bad : colours.warning : colours.ok,
        'title': `${battle.player1.name}'s ${battle.pokemon1.name}`,
        'fields': [
            {
                'title': 'Move 1',
                'value': battle.pokemon1.moves[0].name,
                'short': true
            },
            {
                'title': 'Move 2',
                'value': battle.pokemon1.moves[1].name,
                'short': true
            },
            {
                'title': 'HP',
                'value': battle.pokemon1.hp,
                'short': true
            }
        ],
        'image_url': 'https://img.pokemondb.net/sprites/black-white/anim/normal/'+battle.pokemon1.name+'.gif',
    },{
        'fallback': 'Pokemon are fighting!',
        'color': battle.pokemon2.hp < 50 ? battle.pokemon2.hp < 20 ? colours.bad : colours.warning : colours.ok,
        'title': `${battle.player2.name}'s ${battle.pokemon2.name}`,
        'fields': [
            {
                'title': 'Move 1',
                'value': battle.pokemon2.moves[0].name,
                'short': true
            },
            {
                'title': 'Move 2',
                'value': battle.pokemon2.moves[1].name,
                'short': true
            },
            {
                'title': 'HP',
                'value': battle.pokemon2.hp,
                'short': true
            }
        ],
        'image_url': 'https://img.pokemondb.net/sprites/black-white/anim/normal/'+battle.pokemon2.name+'.gif',
    }];

    return JSON.stringify(msg);
}

function sendFormattedMessage(message, channel){

    let body = '';
    body += 'token=xoxb-97924837456-ntt4oXSCQpyVGNroaNggh2h3';
    body += '&';
    body += 'channel='+channel;
    body += '&';
    body += 'as_user=true';
    body += '&';
    body += 'attachments='+encodeURIComponent(message);

    return fetch('https://slack.com/api/chat.postMessage?'+body, { method: 'GET' });

}

export default function sendStatusMessage(battle, channel){
    sendFormattedMessage(getBattleStatusMessage(battle), channel);
}
