import store from './store';
import { actionTypes as battleActionTypes, actions as battleActions } from './state/battles';
import { actionTypes as audioTypes, actions as audioActions } from './state/audio';
import { actionTypes as botTypes, actions as botActions } from './state/bot';
import getRandomInt from './util/getRandomInt';

export function fresh(id, player1, player2, pokemon1, pokemon2, turn) {
    return {
        id,
        player1,
        player2,
        pokemon1,
        pokemon2,
        turn
    };
}

export function battleFor(player) {
    if (isBattleFor(player)) {
        const state = store.getState();
        return state.battles.filter(b => b.player1.name === player.name || b.player2.name === player.name)[0];
    }
}

export function isBattleFor(player) {
    const state = store.getState();
    return state.battles &&
        state.battles
        .filter(b => b.player1.name === player.name || b.player2.name === player.name)
        .length > 0;
}

export function createBattle(player1, player2) {
    store.dispatch(battleActions[battleActionTypes.BATTLE_START](player1, player2)).then(() => {
        store.dispatch(botActions[botTypes.BOT_DM](player1, `starting a battle with ${player2.name}`));
        store.dispatch(botActions[botTypes.BOT_DM](player2, `starting a battle with ${player1.name}`));
        store.dispatch(audioActions[audioTypes.AUDIO_PLAY](player1.name, 'battle'));
        store.dispatch(audioActions[audioTypes.AUDIO_PLAY](player2.name, 'battle'));
        checkForAIMove();
    });
}

export function handleMove(player, moveString) {
    const battles = store.getState().battles;
    const battle = battles.filter(b => b.player1.name === player.name || b.player2.name === player.name)[0];
    const pokemon = battle.player1.name === player.name ? battle.pokemon1 : battle.pokemon2;
    const move = pokemon.moves.filter(m => m.name === moveString)[0];

    store.dispatch(battleActions[battleActionTypes.BATTLE_MOVE](player, move));

    const updatedBattles = store.getState().battles;
    const updatedBattle = updatedBattles.filter(b => b.player1.name === player.name || b.player2.name === player.name)[0];

    if(updatedBattle.pokemon1.hp <= 0){
        store.dispatch(battleActions[battleActionTypes.BATTLE_END](updatedBattle.id));
        store.dispatch(battleActions[battleActionTypes.BATTLE_WON](updatedBattle.player2));
        store.dispatch(battleActions[battleActionTypes.BATTLE_LOST](updatedBattle.player1));

    }else if(updatedBattle.pokemon2.hp <= 0){
        store.dispatch(battleActions[battleActionTypes.BATTLE_END](updatedBattle.id));
        store.dispatch(battleActions[battleActionTypes.BATTLE_WON](updatedBattle.player1));
        store.dispatch(battleActions[battleActionTypes.BATTLE_LOST](updatedBattle.player2));

    } else {
        store.dispatch(battleActions[battleActionTypes.BATTLE_UPDATE](battle.id));

        //If the AI needs to perform a move then do it
        checkForAIMove();
    }

}

function checkForAIMove(){

    const updatedBattles = store.getState().battles;
    const updatedBattle = updatedBattles.filter(b => b.player1.name === "pokebot" || b.player2.name === "pokebot");

    if(updatedBattle.length == 0) return;

    var battle = updatedBattle[0];

    setTimeout(function(){
        if(
            (battle.player1.name == 'pokebot' && battle.turn === '1')
        ){
            handleMove(battle.player1, battle.pokemon1.moves[getRandomInt(0,2)].name);
        }else if(
            (battle.player2.name == 'pokebot' && battle.turn === '2')
        ){
            handleMove(battle.player2, battle.pokemon2.moves[getRandomInt(0,2)].name);
        }
    }, 3000);
}
