import { actionTypes, actions } from './state/pokemon';
import store from './store';
import pokemonRepo, { limit } from './pokemonRepo';
import getRandomInt from './util/getRandomInt';

export function fresh(p) {
    return {
        name: p.name,
        hp: p.hp,
        at: p.at,
        df: p.df,
        sat: p.sat,
        sdf: p.sdf,
        moves: new Array(...p.moves)
    };
}

export function describe(pokemon) {
    return `${pokemon.name}: ${pokemon.hp}hp left. moves: ${pokemon.moves.map(m => `${m.name}: ${m.damage} damage`).join(', ')}`;
}

async function cachedGetRandomPokemon() {
    const pokemon = await pokemonRepo.getRandomPokemon();
    store.dispatch(actions[actionTypes.POKEMON_ADD](pokemon));
    return pokemon;
}

export default async () => {
    const localPokemon = store.getState().pokemon;
    if (localPokemon && localPokemon.length < 2) {
        return cachedGetRandomPokemon();
    } else {
        const diceRoll = getRandomInt(0, limit);
        if (diceRoll < localPokemon.length) {
            return localPokemon[diceRoll];
        } else {
            return  cachedGetRandomPokemon();
        }
    }
};
