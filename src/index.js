import 'babel-polyfill';
import store from './store';

import express from 'express';
import { resolve } from 'path';
import { actions as audioActions, actionTypes as audioTypes } from './state/audio';
import { actions as botActions, actionTypes as botActionTypes } from './state/bot';

store.dispatch(botActions[botActionTypes.BOT_CONNECT]());
store.dispatch(audioActions[audioTypes.AUDIO_CONNECT]());

const app = express();

app.get('/sound', (req, res) => {
    res.sendFile(resolve(__dirname, '..', 'pages/audio.html'));
});

app.listen(8888);
