import fetch from 'node-fetch';

export const limit = 151;

export default {
    getRandomPokemon: function getRandomPokemon() {

        let selectedPokemonId = Math.ceil(Math.random()*limit);

        return fetch('http://pokeapi.co/api/v2/pokemon/' + selectedPokemonId)
        .then(res => res.json())
        .then((parsed) => {

            var pokemon = {
                name: parsed.name,
                hp: parsed.stats.filter(a => a.stat.name == 'hp')[0].base_stat,
                at: parsed.stats.filter(a => a.stat.name == 'attack')[0].base_stat,
                df: parsed.stats.filter(a => a.stat.name == 'defense')[0].base_stat,
                sat: parsed.stats.filter(a => a.stat.name == 'special-attack')[0].base_stat,
                sdf: parsed.stats.filter(a => a.stat.name == 'special-defense')[0].base_stat,
                moves: []
            };

            let randomMove1 = parsed.moves[Math.round(Math.random()*(parsed.moves.length-1))].move.url.split('/')[6];
            let randomMove2 = parsed.moves[Math.round(Math.random()*(parsed.moves.length-1))].move.url.split('/')[6];

            return Promise.all([this.getMoveDetails(randomMove1), this.getMoveDetails(randomMove2)]).then(([move1, move2]) => {

                pokemon.moves.push(move1);
                pokemon.moves.push(move2);

                return pokemon;
            });

        });
    },

    getMoveDetails: function getMoveDetails(id) {
        return fetch('http://pokeapi.co/api/v2/move/' + id)
        .then(res => res.json())
        .then(function(parsed) {

            var move = {
                name: parsed.name,
                damage: parsed.hasOwnProperty('power') && parsed.power !== null ? parsed.power : 0
            };

            return move;

        });
    }
};
