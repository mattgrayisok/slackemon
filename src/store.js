/* eslint no-console: 0 */
import { createStore, combineReducers, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import botListensMiddleware from './middleware/bot-listens';
import botIncomingMiddleware from './middleware/bot-incoming';
import audioMiddleware from './middleware/audio';
import { inspect } from 'util';

import pokemon from './state/pokemon';
import battles from './state/battles';
import audio from './state/audio';
import players from './state/players';
import bot from './state/bot';

const reducer = combineReducers({ bot, pokemon, battles, audio, players });

const logger = store => next => action => {
    console.log(`running action: ${action.type}`);
    console.log(`state before: ${inspect(store.getState(), { depth: 1 })}`);
    const r = next(action);
    console.log(`state after: ${inspect(store.getState(), { depth: 1 })}`);
    return r;
};


export default createStore(reducer, applyMiddleware(promiseMiddleware, botIncomingMiddleware, botListensMiddleware, audioMiddleware, logger));
