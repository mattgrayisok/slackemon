import makeAction from '../util/makeAction.js';

function pokemonActionName(str) {
    return `pokebot/POKEMON_${str}`;
}

export const actionTypes = {
    POKEMON_ADD: pokemonActionName('add')
};

export const actions = {
    [actionTypes.POKEMON_ADD]: pokemon => makeAction(actionTypes.POKEMON_ADD, pokemon)
};

const actionHandlers = state => ({
    [actionTypes.POKEMON_ADD]: pokemon => state.concat(pokemon)
});

export const defaultState = [];

export default (state = defaultState, action) => {
    const handler = actionHandlers(state);
    if (handler.hasOwnProperty(action.type)) {
        return handler[action.type](action.payload);
    } else {
        return state;
    }
};
