import makeAction from '../util/makeAction.js';
import reducer from './reducer';

function playerActionName(str) {
    return `pokebot/PLAYER_${str}`;
}

export const actionTypes = {
    PLAYER_ADD: playerActionName('add'),
    PLAYER_REMOVE: playerActionName('remove'),
    PLAYER_MESSAGE: playerActionName('message')
};

export const actions = {
    [actionTypes.PLAYER_ADD]: player => makeAction(actionTypes.PLAYER_ADD, player),
    [actionTypes.PLAYER_REMOVE]: playerName => makeAction(actionTypes.PLAYER_REMOVE, playerName),
    [actionTypes.PLAYER_MESSAGE]: (id, message) => makeAction(actionTypes.PLAYER_MESSAGE, { id, message })
};

const actionHandlers = state => ({
    [actionTypes.PLAYER_ADD]: player => Object.assign({}, state, { [player.name]: player }),
    [actionTypes.PLAYER_REMOVE]: playerName => Object.keys(state).filter(n => n !== playerName).reduce((m, x) => {
        m[x] = state[x];
        return m;
    }, {})
});

export const defaultState = {};

export default reducer(defaultState, actionHandlers);
