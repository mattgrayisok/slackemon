import makeAction from '../util/makeAction.js';
import reducer from './reducer';
import store from '../store';
import io from 'socket.io';
import { clone } from 'ramda';

function audioActionName(str) {
    return `pokebot/AUDIO_${str}`;
}

export const actionTypes = {
    AUDIO_CONNECT: audioActionName('conn'),
    AUDIO_ADD_USER: audioActionName('add'),
    AUDIO_REM_USER: audioActionName('rem'),
    AUDIO_PLAY: audioActionName('play')
};

export const actions = {
    [actionTypes.AUDIO_CONNECT]: () => makeAction(actionTypes.AUDIO_CONNECT, undefined),
    [actionTypes.AUDIO_ADD_USER]: (user, soc) => makeAction(actionTypes.AUDIO_ADD_USER, { user, soc }),
    [actionTypes.AUDIO_REM_USER]: socket => makeAction(actionTypes.AUDIO_REM_USER, socket),
    [actionTypes.AUDIO_PLAY]: (user, sound) => makeAction(actionTypes.AUDIO_PLAY, { user, sound })
};

const actionHandlers = ({ socket, connectedUsers }) => ({
    [actionTypes.AUDIO_CONNECT]: () => {
        const s = io(8080);
        s.on('connection', __socket => {
            __socket.on('listen', user => store.dispatch(actions[actionTypes.AUDIO_ADD_USER](user, __socket)));
            __socket.on('disconnect', () => store.dispatch(actions[actionTypes.AUDIO_REM_USER](__socket)));
        });
        return {
            socket: s,
            connectedUsers
        };
    },
    [actionTypes.AUDIO_ADD_USER]: ({ user, soc }) => {

        const newConnectedUsers = clone(connectedUsers);
        newConnectedUsers[user] = soc;

        return {
            socket,
            connectedUsers: newConnectedUsers
        };
    },
    [actionTypes.AUDIO_REM_USER]: s => {
        const currentConnections = Object.keys(connectedUsers).map(name => [name, connectedUsers[name]]);
        return {
            socket,
            connectedUsers: currentConnections
                .filter(([, connection]) => {
                    return connection === s;
                })
                .reduce((obj, [name, connection]) => {
                    obj[name] = connection;
                    return obj;
                }, {})
        };
    }
});

export const defaultState = {
    socket: null,
    connectedUsers: {},
};

export default reducer(defaultState, actionHandlers);
