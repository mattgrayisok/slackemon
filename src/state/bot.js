import makeAction from '../util/makeAction.js';
import reducer from './reducer';
import store from '../store';
import { RtmClient,
         RTM_EVENTS,
         CLIENT_EVENTS,
         MemoryDataStore } from '@slack/client';

const token =  'xoxb-97924837456-ntt4oXSCQpyVGNroaNggh2h3';

function botActionName(str) {
    return `pokebot/BOT_${str}`;
}

export const actionTypes = {
    BOT_CONNECT: botActionName('conn'),
    BOT_INCOMING: botActionName('incoming'),
    BOT_MESSAGE: botActionName('msg'),
    BOT_DM: botActionName('dm')
};

export const actions = {
    [actionTypes.BOT_CONNECT]: () => {
        const rtm = new RtmClient(token, { dataStore: new MemoryDataStore() });
        return new Promise(res => {
            rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, () => res(makeAction(actionTypes.BOT_CONNECT, rtm)));
            rtm.on(RTM_EVENTS.MESSAGE, msg => store.dispatch(actions[actionTypes.BOT_INCOMING](msg)));
            rtm.start();
        });
    },
    [actionTypes.BOT_INCOMING]: msg => makeAction(actionTypes.BOT_INCOMING, msg),
    [actionTypes.BOT_MESSAGE]: (id, message) => makeAction(actionTypes.BOT_MESSAGE, { id, message }),
    [actionTypes.BOT_DM]: (user, message) => makeAction(actionTypes.BOT_DM, { user, message })
};

const actionHandlers = state => ({
    [actionTypes.BOT_CONNECT]: rtm => ({ rtm }),
    [actionTypes.BOT_MESSAGE]: ({ id, message }) => {
        state.rtm && state.rtm.sendMessage(message, id);
        return state;
    },
    [actionTypes.BOT_DM]: ({ user, message }) => {
        state.rtm && state.rtm.sendMessage(message, state.rtm.dataStore.getDMByName(user.name).id);
        return state;
    }
});

const defaultState = { rtm: {} };

export default reducer(defaultState, actionHandlers);
