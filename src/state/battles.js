import makeAction from '../util/makeAction.js';
import reducer from './reducer';
import getRandomPokemon, { fresh as freshPokemon } from '../pokemon';
import { fresh as freshBattle } from '../battle';
import { v4 as getUUID } from 'node-uuid';
import getRandomInt from '../util/getRandomInt';
import store from '../store';
import damageCalculator from '../util/damageCalculator';

function battleActionName(str) {
    return `pokebot/BATTLE_${str}`;
}

export const actionTypes = {
    BATTLE_START: battleActionName('start'),
    BATTLE_END: battleActionName('end'),
    BATTLE_MOVE: battleActionName('move'),
    BATTLE_RUN: battleActionName('run'),
    BATTLE_RAN: battleActionName('ran'),
    BATTLE_WON: battleActionName('won'),
    BATTLE_LOST: battleActionName('lost'),
    BATTLE_WRONG_TURN: battleActionName('wrng'),
    BATTLE_UPDATE: battleActionName('update')
};

export const actions = {
    [actionTypes.BATTLE_START]: async (player1, player2) => {
        const [pokemon1, pokemon2] = await Promise.all([getRandomPokemon(), getRandomPokemon()]);
        const turn = getRandomInt(0, 2) === 0 ? '1' : '2';
        return makeAction(actionTypes.BATTLE_START, {player1, player2, pokemon1, pokemon2, turn});
    },
    [actionTypes.BATTLE_END]: id => makeAction(actionTypes.BATTLE_END, id),
    [actionTypes.BATTLE_MOVE]: (player, move) => makeAction(actionTypes.BATTLE_MOVE, { player, move }),
    [actionTypes.BATTLE_RUN]: player => makeAction(actionTypes.BATTLE_RUN, player),
    [actionTypes.BATTLE_WRONG_TURN]: player => makeAction(actionTypes.BATTLE_WRONG_TURN, player),
    [actionTypes.BATTLE_RAN]: player => makeAction(actionTypes.BATTLE_RAN, player),
    [actionTypes.BATTLE_WON]: player => makeAction(actionTypes.BATTLE_WON, player),
    [actionTypes.BATTLE_LOST]: player => makeAction(actionTypes.BATTLE_LOST, player),
    [actionTypes.BATTLE_UPDATE]: id => makeAction(actionTypes.BATTLE_UPDATE, id)
};

function handleMove(battle, player, move) {
    let pokemon1 = freshPokemon(battle.pokemon1);
    let pokemon2 = freshPokemon(battle.pokemon2);
    const turn = battle.turn;

    if (player.name === battle.player1.name) {
        const baseDamage = damageCalculator(pokemon1, pokemon2, move);
        pokemon2.hp = pokemon2.hp - baseDamage;
    } else {
        const baseDamage = damageCalculator(pokemon2, pokemon1, move);
        pokemon1.hp = pokemon1.hp - baseDamage;
    }

    return freshBattle(battle.id, battle.player1, battle.player2, pokemon1, pokemon2, turn === '1' ? '2' : '1');
}

function attemptRun(battle, player) {
    if (getRandomInt(0, 2) === 0) {
        store.dispatch(actions[actionTypes.BATTLE_RAN](player));
        store.dispatch(actions[actionTypes.BATTLE_END](battle.id));
    }
    return battle;
}

function findBattle(battles, player) {
    return battles.filter(b => b.player1.name === player.name || b.player2.name === player.name)[0];
}

export function rightTurn({ player1, player2, turn }, player) {
    return turn === '1' && player1.name === player.name || turn === '2' && player2.name === player.name;
}

const actionHandlers = state => ({
    [actionTypes.BATTLE_START]: function({player1, player2, pokemon1, pokemon2, turn}) {
        const battle = freshBattle(getUUID(), player1, player2, pokemon1, pokemon2, turn);
        return state.concat(battle);
    },
    [actionTypes.BATTLE_END]: id => state.filter(b => b.id !== id),
    [actionTypes.BATTLE_MOVE]: ({ player, move }) => {
        let applicableBattle = findBattle(state, player);
        if (rightTurn(applicableBattle, player)) {
            const newBattleState = handleMove(applicableBattle, player, move);
            return state.filter(b => b.id !== applicableBattle.id).concat(newBattleState);
        } else {
            return state;
        }
    },
    [actionTypes.BATTLE_RUN]: player => attemptRun(findBattle(state, player))
});

export const defaultState = [];

export default reducer(defaultState, actionHandlers);
