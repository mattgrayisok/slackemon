export default (defaultState, actionHandlers) => {
    return (state = defaultState, action) => {
        const handler = actionHandlers(state);
        if (handler.hasOwnProperty(action.type)) {
            return handler[action.type](action.payload);
        } else {
            return state;
        }
    };
};
